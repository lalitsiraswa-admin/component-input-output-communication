import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { User } from '../../models/user';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  userName: string = '';
  userAge: string = '';
  constructor() { }
  @Output()
  addUserEventEmitter: EventEmitter<User> = new EventEmitter();

  ngOnInit(): void {
  }
  addUser(){
    let user: User = {
      name: this.userName,
      age: this.userAge
    }
    this.addUserEventEmitter.emit(user);
  }
}
